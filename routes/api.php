<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get-all-recipe', 'RecipeController@getRecipe');
Route::get('/get-one-recipe/{id}', 'RecipeController@getOneRecipe');
Route::get('/get-all-type', 'RecipeController@getAllType');
Route::get('/get-recipes/filter/{type}','RecipeController@getFilteredRecipe');
Route::get('/delete-recipe/{id}', 'RecipeController@deleteRecipe');
Route::post('/add-data','RecipeController@addRecipe');
Route::post('/update-data','RecipeController@updateRecipe');
Route::post('/update-image','RecipeController@updateImage');

// Test route
Route::get('/test/add-data','TestController@addRecipe');
Route::get('/test/add-type','TestController@addType');
Route::post('/test/upload-image','TestController@testUploadImage');
