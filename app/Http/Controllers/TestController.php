<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\RecipeDetail;
use App\RecipeType;

class TestController extends Controller
{
    //
    public function addRecipe()
    {
        $recipe = new RecipeDetail;
        $recipe->name = "Beef chow mein";
        $recipe->ingredients = "1 tbsp cornflour,
        2 tbsp dark soy sauce,
        2 tbsp oyster sauce,
        4 tbsp beef stock,
        2 tbsp brown sugar,
        1 tsp sesame oil,
        ½ tsp freshly ground black pepper, or to taste,
        1 tbsp vegetable oil,
        4 garlic cloves, crushed,
        5cm/2in piece fresh ginger, peeled and cut into thin strips,
        2 carrots, peeled and julienned,
        200g/7oz mangetout,
        450g/1lb cooked egg noodles,
        200g/7oz beansprouts,
        2 tbsp sesame oil,
        8 spring onions, peeled and chopped,";
        $recipe->steps = "Just insert steps";
        $recipe->imagePath = "none";
        $recipe->type = "dishes";
        $recipe->save();
    }

    public function addType(){
        $type = new RecipeType;
        $type->name = "dishes";
        $type->save();
    }

    public function testUploadImage(Request $request){
        $image = $request->input('base64');
        Storage::disk('public')->put("testimage.jpeg",base64_decode($image));
        return response()->json($request);
    }
}
