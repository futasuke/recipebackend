<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\RecipeDetail;
use App\RecipeType;

class RecipeController extends Controller
{
    //
    public function getRecipe(){
        $recipeDetail = RecipeDetail::all();
        return response()->json($recipeDetail);
    }

    public function getAllType(){
        $type = RecipeType::all();
        return response()->json($type);
    }

    public function getFilteredRecipe($type){
        $recipeDetail = RecipeDetail::where('type',$type)->get();
        return response()->json($recipeDetail);
    }

    public function addRecipe(Request $request)
    {
        $image = $request->input('base64');
        $imageName = $request->name.".".$request->input('filetype');
        $imagePath = "/storage/".$imageName;
        Storage::disk('public')->put($imageName,base64_decode($image));

        $recipe = new RecipeDetail;
        $recipe->name = $request->name;
        $recipe->ingredients = $request->ingredients;
        $recipe->steps = $request->steps;
        $recipe->imagePath = $imagePath;
        $recipe->type = $request->type;
        $recipe->save();

        return response()->json("Success");
    }

    public function updateRecipe(Request $request){
        $recipe = RecipeDetail::where('id',$request->id)->first();
        $recipe->name = $request->name;
        $recipe->ingredients = $request->ingredients;
        $recipe->steps = $request->steps;
        $recipe->save();
        return response()->json("Success");
        // return response()->json($recipe);
    }

    public function updateImage(Request $request){
        $image = $request->input('base64');
        $imageName = $request->name.".".$request->filetype;
        $imagePath = "/storage/".$imageName;
        if (Storage::disk('public')->exists($request->name.".".$request->filetype)) {
            // ...
            Storage::disk('public')->delete($request->name.".".$request->filetype);
            // return response()->json("Ada file");
        }
        Storage::disk('public')->put($imageName,base64_decode($image));

        $recipe = RecipeDetail::where('id',$request->id)->first();
        $recipe->imagePath = $imagePath;
        $recipe->save();

        return response()->json($imageName);
    }

    public function getOneRecipe($id){
        $recipe = RecipeDetail::where('id',$id)->first();
        return response()->json($recipe);
    }

    public function deleteRecipe($id){
        $recipe = RecipeDetail::where('id',$id)->first();
        $recipe->delete();
        return response()->json("Success");
    }

}
